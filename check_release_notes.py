import re

tickets_commited = set()
tickets_mentioned = set()

def processLineCommits(line):
    # line = re.split(', \)\(\n', line)
    line = re.split('(;|,|\*|\n|\s|\(|\))', line)
    # print(line)
    for word in line:
        if word:
            if word[0] == "#" or word[0] == "!" or ( len(word) > 2 and  word[0:2] == "EE"):
                tickets_commited.add(word)

def processLineNotes(line):
    # line = re.split(', \)\(\n', line)
    line = re.split('(;|,|\*|\n|\s|\(|\))', line)
    # print(line)
    for word in line:
        if word:
            if word[0] == "#" or word[0] == "!" or ( len(word) > 2 and  word[0:2] == "EE"):
                tickets_mentioned.add(word)

def readfileCommits():
    f = open("commit_log.txt", "r")
    for line in f:
        processLineCommits(line)

def readfileNotes():
    f = open("notes_added.txt", "r")
    for line in f:
        processLineNotes(line)


def main():

    readfileCommits()
    print("tickets_commited: ", sorted(tickets_commited))
    readfileNotes()
    print("tickets_mentioned: ", sorted(tickets_mentioned))

    not_found = []
    for ticket in tickets_commited:
        if not ticket in tickets_mentioned:
            not_found.append(ticket)

    not_found = sorted(not_found)
    print("tickets not found in the notes: ", end="")
    print(not_found)
    print("Links:")
    for ticket in not_found:
        if ticket[0] == "#" or ticket[0] == "!":
            print("https://gitlab.com/eyeo/adblockplus/abc/webext-sdk/-/issues/" + ticket[1:])
        if ticket[0:2] == "EE":
            print("https://jira.eyeo.com/browse/"+ticket)
main()
