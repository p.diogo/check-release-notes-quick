# Check Release Notes Quick

## Getting started

This is a script to check the release notes of your project. It looks at the ticket numbers mentioned in the release notes and compares it with the ticket numbers mentioned in the commit logs of your project.

It recognizes both Jira (EE-00 format) and Gitlab (#000 format) tickets. It automatically gives you the link to the tickets missing in your release notes.

## Running the code

To run the code you will need two input files:

* `notesAdded.txt` should contain all the new text you will add in your `RELEASE_NOTES.md` file.
* `commit_log.txt` should contain all the commits added to the master branch since the last release. You can generate this data with the following command: `git log <commit-id>.. --oneline > commit_log.txt`

To execute the code just run `python3 check_release_notes.py`

## Example

#### `commit_log.txt`
```
ecbbf701 Fix #100 - Very cool feature
775bb3ba Fix #101 - Solved a bug
```


#### `notes_added.txt`
```
- Added very cool feature (#100)
```

#### Output
```
tickets_commited:  ['#100', '#101']
tickets_mentioned:  ['#100']
tickets not found in the notes: ['#101']
Links:
https://gitlab.com/eyeo/adblockplus/abc/webext-sdk/-/issues/101
```
